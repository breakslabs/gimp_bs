# Gimp BS

BS Labs stuff for GIMP. Mostly just the one script, right now. It's pretty
awesome, though. You should lick it. Go ahead, no one's looking.


Most plug-ins require gimp-python support. Please be sure it is enabled
before attempting to install or use any of the included plug-ins. If unsure,
try "**Filters -> Python-Fu -> Console**" in the GIMP. If that option is
present and you can successfully open the console without errors, GIMP python
support is installed and enabled. If not, you have some Googling to do.

## Installation

Plug-in installation simply involves copying the appropriate plug-in file to
the GIMP "plug-ins" directory appropriate for your system. For GNU/Linux or
BSD users, this is typically "~/.gimp-\<version\>/plug-ins/"
(e.g. "~/gimp-2.8/plug-ins/"). For Microsoft Windows users, it may be
"C:\Windows\Users\\<username\>\AppData\Roaming\GIMP\\<version\>\plug-ins". If
unsure, go to "**File -> Preferences -> Folders -> Plugins**" in the GIMP. If
multiple paths are present, you probably want the one that is in your user
folder.


At present, all plug-ins are single Python files. So, for example, to install
the Vector CRT Glow plug-in, just copy the "crt_glow.py" file from the
"plugins/vector_crt" directory to your plug-ins folder as described above.


A _very_ rudimentary installer script for UNIX-like environments is available
in the plugins directory. Run it without arguments for help.

## Plug-ins

Plug-ins are tested using GIMP 2.8. I realize this is an older version of
GIMP, but I don't have a more current version handy at present. If you have
issues with the scripts, please open an issue on the GitLab issue tracker.


### Vector CRT Effect


**Filters->Artistic->Vector CRT Glow**


A filter that attempts to invoke a CRT vector monitor look when given a
monochrome image. Lots of knobs to twidle. Disable "Cleanup" to leave the
layers intact for additional post-tweaking.
