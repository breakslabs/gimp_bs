#!/bin/sh

# It's a script, all right.

usage() {
    cat <<EOF

Usage:
   ${0} all | <plug-in directory> [<plug-in directory>...]
    
E.g.: \"${0} vector_crt\""
    
The environment variable 'GIMP_RCDIR' may be set to the parent directory where
plug-ins should be installed (e.g.  '~/.gimp-2.8'). The script will install
plug-ins in '\$GIMP_RCDIR/plug-ins' if that directory exists, or create it if
necessary. If GIMP_RCDIR is not set, the installer will attempt to query GIMP
for the correct location.
EOF
    exit 0
}

error() {
    echo "${1}"
    exit 1
}

install_plugin() {
    target=${1%%/}
    pdir=${2%%/}
    [ -d "${target}" ] || error "Invalid plug-in target '${target}'"
    [ -d "${pdir}" ] || error "Invalid plug-in folder '${pdir}'"
    if [ -x "${target}"/install.sh ]; then
	pushd "${target}"
	./install.sh "${pdir}" || error "Install failed"
	popd
    else
	cp -f "${target}"/*.py "${pdir}" || error "Could not copy plugin"
    fi
}

install_all() {
    pdir="${1}"
    while IFS= read -r target
    do
	echo -n "Installing '${target}'... "
	install_plugin "${target}" "${pdir}"
	echo "done"
    done <<EOF
$(find . -maxdepth 1 -type d -not -name "." -not -name "..")
EOF
}

if [ ${#} -eq 0 ]; then usage ${0}; fi

if [ -z "${GIMP_RCDIR}" ]; then
    echo -n "Querying GIMP for user rc directory. This may take a bit... "
    GIMP_RCDIR=$(timeout 30 gimp -i --batch-interpreter=python-fu-eval \
		      -b 'print(gimp.personal_rc_file(""));gimp.exit()') \
	      || error "Cannot determine GIMP rc directory"
    echo -e "\nInstaller will install plugins in '${GIMP_RCDIR}/plug-ins'."
    read -p "Press enter to continue, ^C to abort:" foo
fi

if [ ! -d "${GIMP_RCDIR}" ]; then
    echo -e  "\nERROR: GIMP rc directory '${GIMP_RCDIR}' does not exist." 
    exit 1
fi

if [ ! -d "${GIMP_RCDIR}/plug-ins" ]; then
    mkdir "${GIMP_RCDIR}/plug-ins" || error "Cannot create 'plug-ins' directory"
fi
plugin_dir="${GIMP_RCDIR}/plug-ins"

if [ ${#} -eq 1 ] && [ ${1} = 'all' ]; then install_all "${plugin_dir}"; exit 0; fi

while [ ${#} -gt 0 ]; do
    key="${1}"
    shift
    if [ -d ./"${key}" ]; then
	echo -n "Installing '${key}'... "
	install_plugin "${key}" "${plugin_dir}"
	echo "done"
    else
	echo "No such plug-in: '${key}'"
    fi
done


