#!/usr/bin/python
"""Try to make a monochrome image look like a vector CRT display.

Expects a monochromatic on "black" image (e.g. green or amber).
Defaults seem pretty okay. Messing with the blur radii (especially glow) may
be fun. If "Clean up" is turned off, layers will not be merged, and undo will
not be atomic. 

Have fun - Devoid (Break Shit Labs)
"""
import gimpfu as gf

pdb = gf.pdb

# Layer stack:
#
#   C (Plasma dirt)
#   B (Glow)
#   A (Smooth/blur)
#   D (Wash)
#   Original

def vector_crt_glow(timg, tdrawable, posterize=0, plas_seed=0xdeadbeef,
                    glow_rad=5.0, smooth_rad=2.0,  washout=1.0,
                        wash_color=(255, 255, 255), cleanup=True):
    width, height = (tdrawable.width, tdrawable.height)
    glow = tdrawable.copy(True)
    blur = tdrawable.copy(True)
    wash = pdb.gimp_layer_new(timg, width, height, gf.RGB_IMAGE,
                               'Wash Adjustment', washout, gf.ADDITION_MODE)
    plas = pdb.gimp_layer_new(timg, width, height, gf.RGB_IMAGE,
                              'Plasma dirt', 22.0, gf.BURN_MODE)
    if cleanup:
        pdb.gimp_image_undo_group_start(timg)
    pdb.gimp_context_push()
    timg.add_layer(wash, 0)
    timg.add_layer(blur, 0)
    timg.add_layer(glow, 0)
    timg.add_layer(plas, 0)
    
    # Layer A (Smooth layer)
    pdb.plug_in_gauss_rle2(timg, blur, smooth_rad, smooth_rad)
    pdb.plug_in_gauss_rle2(timg, blur, smooth_rad+0.5, smooth_rad+0.5)
    blur.opacity = 80.0
    blur.mode = gf.ADDITION_MODE
    blur.name = 'Smooth'
    
    # Layer B (Glow layer)
    if posterize:
        pdb.gimp_posterize(glow, posterize)
    pdb.plug_in_neon(timg, glow, 7.0, 0.08)
    pdb.plug_in_gauss_rle2(timg, glow, glow_rad, glow_rad)
    glow.mode = gf.ADDITION_MODE
    glow.opacity = 17.0
    glow.name = 'Glow'
    
    # Layer C (Plasma layer)
    pdb.plug_in_plasma(timg, plas, plas_seed, 1.0)
    pdb.gimp_desaturate_full(plas, 0)
    pdb.plug_in_gauss_rle2(timg, plas, 30, 30)

    # Layer D (Wash adj)
    pdb.gimp_context_set_foreground(wash_color)
    pdb.gimp_edit_fill(wash, gf.FOREGROUND_FILL)
    
    if cleanup:
        for l in [wash, blur, glow, plas]:
            pdb.gimp_image_merge_down(timg, l, 1)
            #pdb.gimp_image_raise_layer_to_top(timg, l)
        pdb.gimp_image_undo_group_end(timg)
    pdb.gimp_displays_flush()
        
gf.register(
	"python_fu_vector_crt_glow",
	"Give the specified layer a 'CRT glow' effect",
	"Give the specified layer a 'CRT glow' effect",
	"BS Labs",
	"BS Labs",
	"2020",
	"<Image>/Filters/Artistic/V_ector CRT Glow",
	"RGB*", 
	[
		(gf.PF_INT, 'posterize', 'Posterize levels (0 disables)', 8),
        (gf.PF_INT, 'plas_seed', 'Plasma seed', 0xdeadbeef),
        (gf.PF_FLOAT, 'glow_rad', 'Glow radius', 5.0),
        (gf.PF_FLOAT, 'smooth_rad', 'Smooth radius', 2.0),
        (gf.PF_SLIDER, 'washout', 'Washout', 1.0, (0, 100, 0.1)),
        (gf.PF_COLOR, 'wash_color', 'Wash hue', (255, 255, 255)),
        (gf.PF_BOOL, 'cleanup', 'Clean up after ourselves', True),
	],
	[],
    vector_crt_glow)

gf.main()
